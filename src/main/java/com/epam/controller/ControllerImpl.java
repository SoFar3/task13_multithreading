package com.epam.controller;

import com.epam.model.Fibonacci;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ControllerImpl implements Controller {

    private static Logger logger = LogManager.getLogger();

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {

        new Thread(() -> {
            Fibonacci fibonacci = new Fibonacci(30);
            logger.info(fibonacci.generate());
        }).start();

        new Thread(() -> {
            Fibonacci fibonacci = new Fibonacci(20);
            logger.info(fibonacci.generate());
        }).start();

        new Thread(() -> {
            Fibonacci fibonacci = new Fibonacci(10);
            logger.info(fibonacci.generate());
        }).start();

    }

}
