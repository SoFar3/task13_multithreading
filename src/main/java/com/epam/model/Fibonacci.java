package com.epam.model;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fibonacci {

    private int n;

    public Fibonacci() {
        this(10);
    }

    public Fibonacci(int n) {
        this.n = n;
    }

    public List<Integer> generate() {
        return Stream
                .iterate(new int[] {0, 1}, i -> new int[] {i[1], i[0] + i[1]} )
                .map(i -> i[0])
                .limit(n)
                .collect(Collectors.toList());
    }

    public int getN() {
        return n;
    }

    @Override
    public String toString() {
        return "Fibonacci{" +
                "n=" + n +
                '}';
    }

}
