package com.epam.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class ViewImpl implements View {

    private Menu menu;
    private BufferedReader reader;

    public ViewImpl() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public void showMenu() {
        int counter = 1;
        while (true) {
            for (Map.Entry<String, Option> stringOptionEntry : menu.getMenu().entrySet()) {
                System.out.println(String.format("%s - %s", stringOptionEntry.getKey(), stringOptionEntry.getValue().getName()));
            }
            waitForUserOption();
            if (counter >= menu.getRepeat()) {
                if (menu.getMenu().get("N") != null) {
                    menu.getMenu().get("N").getAction().run();
                } else {
                    menu.getMenu().get("Q").getAction().run();
                }
                counter = 0;
            }
            counter++;
        }
    }

    @Override
    public String userInput(String message) {
        String input = "";
        try {
            System.out.println(message);
            input = reader.readLine();
        } catch (IOException ignored) { }
        return input;
    }

    @Override
    public void waitForUserOption() {
        System.out.println("Enter your choice:");
        while (true) {
            try {
                String choice = reader.readLine();
                if (doAction(choice)) {
                    break;
                }
                System.out.println("Incorrect input");
            } catch (IOException ignored) { }
        }
    }

    @Override
    public boolean doAction(String key) {
        Option option = menu.getMenu().get(key);
        if (option == null) {
            return false;
        } else {
            option.getAction().run();
            return true;
        }
    }

}
