package com.epam;

import com.epam.controller.ControllerImpl;

public class App {

    public static void main(String[] args) {
        new ControllerImpl().start();
    }

}
